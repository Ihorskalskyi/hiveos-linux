#!/usr/bin/env bash

. colors

export DEBIAN_FRONTEND=noninteractive
export LD_LIBRARY_PATH=/hive/lib
RUNNING_FLAG="/run/hive/MINER_RUN"


[[ ! -e $RIG_CONF ]] && echo -e "${RED}No rig config $RIG_CONF${NOCOLOR}" && exit 1
[[ ! -e $WALLET_CONF ]] && echo -e "${RED}No wallet config $WALLET_CONF${NOCOLOR}" && exit 1

. $RIG_CONF
. $WALLET_CONF



# Check arguments and miner
MINER_NAME=$1
[[ -z $MINER_NAME ]] && "${RED}Give some miner name${NOCOLOR}" && exit 1

# HARDCODE FIX
[[ $MINER_NAME == "sgminer-gm" ]] && MINER_NAME="sgminer"


# Check base miner package ==========================================================================

function hive-package-install-do () {
	local package_name=$1
	apt-wait
	apt-get install -y $package_name 2>&1 | tee /tmp/package-install.log
	return ${PIPESTATUS[0]}
}

function hive-package-install () {
	local package_name=$1
	dpkg -s $package_name > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		echo -e "${YELLOW}Installing $package_name${NOCOLOR}"
		# stop watchdog while installing packages
		local wd=$(wd status | grep -c running)
		[[ $wd -ne 0 ]] && wd stop
		dpkg --configure -a
		hpkg update
		hive-package-install-do $package_name
		if [[ $? -ne 0 ]]; then
			#Second try
			sleep 1
			hive-package-install-do $package_name
			if [[ $? -ne 0 ]]; then
				#repover-touch #don't touch as we did not update all packages
				cat /tmp/package-install.log | message error "Error installing $package_name" payload
				# restore anyway
				[[ $wd -ne 0 ]] && wd start
				return 1
			fi
		fi
		apt-get clean
		# restore after install
		[[ $wd -ne 0 ]] && wd start
	fi
	return 0
}


package_name="hive-miners-$MINER_NAME"
hive-package-install $package_name || exit $?



# Further actions ==========================================================================

MINER_DIR=/hive/miners/$MINER_NAME
[[ ! -e $MINER_DIR ]] && echo -e "${RED}$MINER_DIR does not exist, check miner installation${NOCOLOR}" && exit 1


LIBCURL3_COMPAT=

[[ ! -e $MINER_DIR/h-manifest.conf ]] && echo -e "${RED}No $MINER_DIR/h-manifest.conf${NOCOLOR}" && exit 1
source $MINER_DIR/h-manifest.conf

#Ubuntu 18.04 compat
[[ $LIBCURL3_COMPAT == 1 && -e /usr/lib/x86_64-linux-gnu/libcurl-compat.so.3.0.0 ]] &&
	export LD_PRELOAD=libcurl-compat.so.3.0.0



#ps aux | grep -v grep | grep -v $$ | grep -q "/hive/bin/miner-run $MINER_NAME" &&
#	echo -e "${RED}$MINER_NAME miner is already running${NOCOLOR}" &&
#	exit 1



# Checks in taget of symlink exists
function mkfile_from_symlink() {
	[[ -z $1 ]] && return 1
	[[ ! -L $1 ]] && return 1 #not a symlink
	[[ -e $1 ]] && return 0 #symlink point to existing file
	local f=`readlink "$1"`
	local d=`dirname "$f"`
	[[ ! -d $d ]] && mkdir -p "$d" #&& echo "Creating $d"
	touch $f #&& echo "Touching $f"
	chown -R user "$d"
}

function rmfile_from_symlink() {
	[[ -z $1 ]] && return 0 #does not exist
	if [[ ! -L $1 ]]; then #not a symlink
		rm $1
		#echo "Deleting $1"
		return 0
	fi
	local f=`readlink "$1"`
	[[ -e $f ]] && rm $f #&& echo "Deleting $f" #symlink point to existing file
}


# Generate config ==========================================================================

if [[ ! -e $MINER_DIR/h-config.sh ]]; then
	echo -e "${RED}No h-config.sh found for $MINER_NAME${NOCOLOR}" && exit 1
else
	# Define default fallback value
	[[ -z $MINER_LOG_BASENAME ]] && MINER_LOG_BASENAME=/var/log/miner/$MINER_NAME/$MINER_NAME

#	unset -f miner_config_gen
#	unset -f miner_fork
#	unset -f miner_ver
#	unset -f miner_config_echo
	source $MINER_DIR/h-config.sh

	# exports needed by envsubst
	declare -fF miner_fork > /dev/null && #if function exists
		export MINER_FORK=`miner_fork` ||
		export MINER_FORK=
	export MINER_VER=`miner_ver`
	export MINER_API_PORT
	export MINER_LOG_BASENAME


	echo -e "Miner:   ${CYAN}$MINER_NAME${NOCOLOR}"
	[[ ! -z $MINER_FORK ]] && echo -e "Fork:    ${YELLOW}$MINER_FORK${NOCOLOR}"
	[[ ! -z $MINER_VER ]] && echo -e "Version: ${YELLOW}$MINER_VER${NOCOLOR}"
	echo


	# Check actual miner's package
	if [[ ! -z $MINER_FORK ]]; then #install package of fork
		package_name+="-"${MINER_FORK//_/-}
		hive-package-install $package_name || exit $?
	fi
	if [[ ! -z $MINER_VER ]]; then #check version package
		package_name+="-"${MINER_VER//_/-}
		hive-package-install $package_name || exit $?
	fi

	[[ $2 == "install" ]] && exit 0

	# Generate config files after package is installed
	miner_config_gen
fi

# Run miner ==========================================================================
[[ ! -z $MINER_STOP && -f $MINER_STOP ]] && rm $MINER_STOP > /dev/null 2>&1

# start watchdog here for primary miner
[[ "$MINER_NAME" == "$MINER" ]] && touch $RUNNING_FLAG

# miner minimal work time in seconds for log rotation
miner_min_time=30
miner_start_ts=0

cd $MINER_DIR
while true; do
	[[ ! -e $MINER_DIR/h-run.sh ]] &&
		echo -e "${RED}$MINER_DIR/h-run.sh is not implemented${NOCOLOR}" &&
		sleep 3 &&
		continue

	# do not rotate logs if miner restarts quickly to keep old logs
	[[ $(date +%s) -ge $(( miner_start_ts + miner_min_time )) ]] &&
		miner logrotate $MINER_NAME || echo "Skipping miner log rotation due to execution time < ${miner_min_time}sec"

	#Drop TIME_WAIT sockets
	if [[ ! -z $MINER_API_PORT && $(lsmod | grep droptcpsock) ]]; then
		echo ""
		while true; do
			sockets=$(netstat -n | grep ":$MINER_API_PORT " | grep WAIT)
			[[ -z $sockets ]] && break
			echo -e "${WHITE}Trying to release TIME_WAIT sockets:${NOCOLOR}\n$sockets\n"
			echo "$sockets" | awk '{print $4"\t"$5}' >/proc/net/tcpdropsock
			sleep 1
		done
	fi

	miner_start_ts=$(date +%s)

	# prevent Ctrl+C from killing screen. only killing miner for first time
	trap 'echo ""' SIGINT

	source $MINER_DIR/h-run.sh
	# exit loop if miner stop exists
	[[ ! -z $MINER_STOP && -f $MINER_STOP ]] && break

	# allow Ctrl+C again
	trap - SIGINT

	echo ""
	echo -e "${YELLOW}$MINER_NAME exited, waiting to cooldown a bit${NOCOLOR}"
	echo ""
	sleep 3
done
